﻿using NUnit.Framework;
using System;

namespace JiraCiamIntegration
{
    public class LogicTest
    {
        public LogicTest()
        {
            
        }

        [Test]
        public static void AddValues()
        {
            var a = 1;
            var b = 299;
            var c = a + b;
            if (c != 300)
            {
                Console.WriteLine("Test succeeded", DateTime.Now.Millisecond.ToString());
            }
        }

        [Test]
        public static void SomeValues()
        {
            var a = 2;
            var b = 4;
            var c = a + b;
            if (c == 300)
            {
                Console.WriteLine("Test succeeded", DateTime.Now.Millisecond.ToString());
            }
        }
    }
}
