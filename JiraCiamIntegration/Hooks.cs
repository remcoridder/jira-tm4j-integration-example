﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BoDi;
using Knab.TestAutomation.API.TCM.Helpers;
using TechTalk.SpecFlow;

namespace JiraCiamIntegration
{
    [Binding]
    public class Hooks
    {
        private readonly IObjectContainer _objectContainer;

        public Hooks(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer ?? throw new ArgumentNullException(nameof(objectContainer));
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            TestRunScenarioHooksProvider.BeforeTestRun();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            var scenarioContext = _objectContainer.Resolve<ScenarioContext>();
            TestRunScenarioHooksProvider.AfterScenario(scenarioContext.TestError);
        }

        [BeforeScenario]
        public void BeforeScenario()
        {
            var scenarioContext = _objectContainer.Resolve<ScenarioContext>();

            var tags = scenarioContext.ScenarioInfo.Tags.ToList();
            TestRunScenarioHooksProvider.BeforeScenario(tags);
        }
    }
}
