﻿using TechTalk.SpecFlow;

namespace JiraCiamIntegration
{
    [Binding]
    public class CalculusSteps
    {
        [Given(@"I add two values")]
        public void GivenIAddTwoValues()
        {
            LogicTest.AddValues();
        }

        [Given(@"I do something with some other values")]
        public void GivenIDoSomethingWithSomeOtherValues()
        {
            LogicTest.SomeValues();
        }
    }
}
